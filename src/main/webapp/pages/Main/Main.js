Application.$controller("MainPageController", ['$rootScope', '$scope', 'Widgets', 'Variables', '$timeout', '$location',
    function($rootScope, $scope, Widgets, Variables, $timeout, $location) {
        "use strict";

        var bannerDisplay;

        /** Cancelling the $timeout when page is destroyed*/
        $scope.$on("$destroy", function() {
            $timeout.cancel(bannerDisplay);
        });

        /** For the promotions banner, we have multiple views and we are showing them one after the other
         * with 3000 ms timegap. The below code snippet is used for this, iterating the views and showing them. */
        var views = ['promo-camera', 'promo-mobile', 'promo-peripheral'],
            bannerTimeout = 3000;

        var displayBannerImage = function(index) {
            for (var i = 0; i < views.length; i++) {
                var viewName = views[i];
                if (index == i) {
                    Widgets[viewName].show = true;
                } else {
                    Widgets[viewName].show = false;
                }
            }

            bannerDisplay = $timeout(function() {
                var n = index + 1;
                if (n == views.length) {
                    n = 0;
                }
                displayBannerImage(n);
            }, bannerTimeout);
        };

        /** This is a callback function, that is called after a page is rendered.*/
        $scope.onPageReady = function() {
            $rootScope.pageLoading = false;
            displayBannerImage(0);
        }

        /** On clicking the category(say smartphone) we load all the products under that category*/
        $scope.onCategoryClick = function(category) {
            $rootScope.navigateToCategory(category);
            $location.path('Category');
            $rootScope.pageLoading = true;
        }

        /** On clicking the product, we navigate to the product detail page*/
        $scope.categorylistClick = function($event, $scope) {
            var currentItem = $scope.item;
            currentItem.imgUrl = currentItem.imgUrl.replace("Thumbnails", "Images");
            $rootScope.selectedItem = currentItem;
            $rootScope.addToLocalStorage("wm.activeProduct", currentItem);
            $location.path("Products");
            $rootScope.pageLoading = true;
        };

        $scope.cameras_listonBeforeUpdate = function(variable, data) {
            WM.forEach(data, function(camera) {
                camera.imgUrl = "http://wmstudio-apps.s3.amazonaws.com/eshop/Products/Thumbnails" + camera.imgUrl;
            });
            return data;
        };

        $scope.smartphones_listonBeforeUpdate = function(variable, data) {
            WM.forEach(data, function(phone) {
                phone.imgUrl = "http://wmstudio-apps.s3.amazonaws.com/eshop/Products/Thumbnails" + phone.imgUrl;
            });
            return data;
        };

        $scope.laptops_listonBeforeUpdate = function(variable, data) {
            WM.forEach(data, function(laptop) {
                laptop.imgUrl = "http://wmstudio-apps.s3.amazonaws.com/eshop/Products/Thumbnails" + laptop.imgUrl;
            });
            return data;
        };

    }
]);


Application.$controller("smartphoneslistController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("cameraslistController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("laptopslistController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);