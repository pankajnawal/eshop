Application.$controller("ProductsPageController", ["$rootScope", "$scope", "Widgets", "Variables", "$location", "DialogService",
    function($rootScope, $scope, Widgets, Variables, $location, DialogService) {
        "use strict";

        /** This is a callback function, that is called after a page is rendered.*/
        $scope.onPageReady = function() {
            $rootScope.pageLoading = false;
            /*checking if product is available.*/
            if (!$rootScope.selectedItem.availability) {
                /*disabling button click if product is out of stock*/
                Widgets.addToCart.disabled = true;
            }
        }

        /**This is a callback function, that is called after the page variables are loaded. */
        $scope.onPageVariablesReady = function() {
            Variables.selectedItem.dataSet = $rootScope.selectedItem;
            delete Variables.currentUser.dataSet.dataValue;
        }

        /*method to handle add to cart click and insert item to db*/
        $scope.addToCartClick = function($event, $scope) {
            if ($rootScope.userLoggedin) {
                $rootScope.pageLoading = true;
                $rootScope.addItemToCart();
            } else {
                $rootScope.loginBeforeCart = true;
                DialogService.showDialog("CommonLoginDialog");
            }
        };
    }
]);